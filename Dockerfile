FROM openjdk:8-stretch
COPY /build/libs/com.ehvlinc.api-gateway-0.0.1-SNAPSHOT.jar run.jar
EXPOSE 8761
CMD "java" "-jar" "run.jar"
