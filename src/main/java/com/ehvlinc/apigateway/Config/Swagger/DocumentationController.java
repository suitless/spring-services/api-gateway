//package com.ehvlinc.apigateway.Config.Swagger;
//
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Primary;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.Component;
//import springfox.documentation.swagger.web.SwaggerResource;
//import springfox.documentation.swagger.web.SwaggerResourcesProvider;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Component
//@Primary
//@EnableAutoConfiguration
//public class DocumentationController implements SwaggerResourcesProvider {
//    /**
//     * This function is a function that is  creating a list of resources which it then returns to swagger for use
//     * The resources are the different services
//     * @return List
//     */
//    @Override
//    public List get() {
//        List resources = new ArrayList<>();
//        resources.add(swaggerResource("module-service", "/modules/v2/api-docs", "2.0"));
//        resources.add(swaggerResource("account-service", "/accounts/v2/api-docs", "2.0"));
//        resources.add(swaggerResource("profile-service", "/profiles/v2/api-docs", "2.0"));
//        resources.add(swaggerResource("cdn-service", "/cdn/v2/api-docs", "2.0"));
//        resources.add(swaggerResource("logging-service", "/log/v2/api-docs", "2.0"));
//        resources.add(swaggerResource("email-service", "/email/v2/api-docs", "2.0"));
//        return resources;
//    }
//
//    private SwaggerResource swaggerResource(String name, String location, String version) {
//        SwaggerResource swaggerResource = new SwaggerResource();
//        swaggerResource.setName(name);
//        swaggerResource.setLocation(location);
//        swaggerResource.setSwaggerVersion(version);
//        return swaggerResource;
//    }
//
//}