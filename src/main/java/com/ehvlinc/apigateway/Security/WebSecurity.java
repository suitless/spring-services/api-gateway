package com.ehvlinc.apigateway.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@EnableWebFluxSecurity
public class WebSecurity  {
    private SecurityContextRepository securityContextRepository;

    @Autowired
    public WebSecurity( SecurityContextRepository securityContextRepository) {
        this.securityContextRepository = securityContextRepository;
    }

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        // Disable default security.
        http.headers().frameOptions().disable();
        http.cors();
        http.csrf().disable();
        http.httpBasic().disable();
        http.formLogin().disable();
        http.logout().disable();

        // Add custom security.
        http.securityContextRepository(securityContextRepository);

        // Disable swagger-ui authentication
        http.authorizeExchange().pathMatchers(
                "/**/v2/api-docs",
                "/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**",
                "/swagger-resources/configuration/ui",
                "/swagge‌​r-ui.html",
                "/swagger-resources/configuration/security").permitAll();

        // Disable authentication for `/auth/**` routes.
        http.authorizeExchange().pathMatchers(HttpMethod.POST,"/accounts/", "/login").permitAll();
        http.authorizeExchange().pathMatchers(HttpMethod.GET,"/cdn/**", "/modules/**").permitAll();
        http.authorizeExchange().pathMatchers(HttpMethod.OPTIONS, "/**").permitAll();
        // Authenticate all other requests.
        http.authorizeExchange().anyExchange().authenticated();

        return http.build();
    }

    @Bean
    CorsWebFilter corsWebFilter() {
        CorsConfiguration corsConfig = new CorsConfiguration();

        corsConfig.setAllowedOrigins(Arrays.asList(
                "http://develop.suitless.nl", "https://suitless.online", "https://suitless.nl", "http://startupseindhoven.nl", "http://startupseindhoven.com", "http://localhost:8080", "http://localhost:4200", "http://68.183.6.25"));
        corsConfig.setMaxAge(3600L);
        corsConfig.setAllowCredentials(true);
        corsConfig.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
        corsConfig.setAllowedHeaders(Arrays.asList("*"));
        corsConfig.setExposedHeaders(Arrays.asList("X-Auth-Token","Authorization","Access-Control-Allow-Origin","Access-Control-Allow-Credentials", "X-Forwarded-Host", "X-Forwarded-Proto", "X-Forwarded-Port"));

        UrlBasedCorsConfigurationSource source =
                new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfig);

        return new CorsWebFilter(source);
    }

}
