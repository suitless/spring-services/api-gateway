package com.ehvlinc.apigateway.Security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utility class used for handling JWT token operations in one central place
 * @author nick van der burgt
 * @version 1.0
 * @since 19-3-2019
 */
@Component
public class JwtUtil implements Serializable {
    @Value("${security.jwtKey}")
    private String secretKey;

    public Claims getAllClaimsFromToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
        return claims;
    }

    public String getUsernameFromToken(String token) {
        return getAllClaimsFromToken(token).getSubject();
    }

    public List<GrantedAuthority> getRolesFromToken(String token) {
        List<String> scopes = (List<String>) getAllClaimsFromToken(token).get("scopes");

        return  scopes.stream()
                .map(authority -> new SimpleGrantedAuthority(authority))
                .collect(Collectors.toList());
    }

    public Date getExpirationDateFromToken(String token) {
        int timeInSeconds = (Integer) getAllClaimsFromToken(token).get("exp");
        long timeInMs = Long.valueOf(timeInSeconds) * 1000;
        try {
            return new Date(timeInMs);
        } catch (NullPointerException e){
            e.printStackTrace();
            return new Date(0);
        }
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public Boolean validateToken(String token) {
        return !isTokenExpired(token);
    }
}
